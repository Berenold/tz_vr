﻿using UnityEngine;

public class CylinderController: MonoBehaviour
{
  #region Variable part
  public LoaderController loaderController;
  public Material whiteMaterial;
  public GameObject tura;
  private CylinderType _type = CylinderType.Default;
  private Material _colorMaterial;
  private EventHolder eventHolder;
  private bool _isColorMaterial = false;
  public bool IsColorMaterial
  {
    get { return _isColorMaterial; }
  }
  #endregion

  private void OnEnable()
  {
    if(ApplicationContainer.Instance.eventHolder != null)
    {
      eventHolder = ApplicationContainer.Instance.eventHolder;
      eventHolder.restoreColorEvent += RestoreColor;
    }

    if(loaderController != null)
    {
      loaderController.InitProgressPar();
    }
    else
    {
      Debug.LogError(string.Format("[{0}][OnEnable]ERROR: loaderController is empty!", this.GetType()));
    }
  }

  private void OnDisable()
  {
    if(eventHolder != null)
    {
      eventHolder.restoreColorEvent -= RestoreColor;
    }
  }

  private void RestoreColor()
  {
    ChangeMaterial(CylinderMaterial.White);
  }

  public void Init(CylinderType type)
  {
    _type = type;

    switch(_type)
    {
      case CylinderType.Green:
        _colorMaterial = ApplicationContainer.Instance.materials[0];
        break;
      case CylinderType.Red:
        _colorMaterial = ApplicationContainer.Instance.materials[1];
        break;
    }
  }

  public void ChangeMaterialOnHit()
  {
    if(!_isColorMaterial)
    {
      ChangeMaterial(CylinderMaterial.Color);
    }
  }

  private void ChangeMaterial(CylinderMaterial newMaterial)
  {
    Renderer renderer = tura.GetComponent<Renderer>();

    if(renderer != null)
    {
      switch(newMaterial)
      {
        case CylinderMaterial.White:
          renderer.sharedMaterial = whiteMaterial;
          _isColorMaterial = false;
        break;
        case CylinderMaterial.Color:
          renderer.sharedMaterial = _colorMaterial;
          _isColorMaterial = true;
          ApplicationContainer.Instance.CheckColor(_type, this.gameObject);
          break;
      }
    }
  }
}

public enum CylinderType
{
  Red = 0,
  Green = 1,
  Default = 2
}

public enum CylinderMaterial
{
  White,
  Color,
  Default
}