﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class ApplicationContainer: Singleton<ApplicationContainer>
{
  #region Variable part
  public Material[] materials;
  public GameObject cylinderPrefab;
  public Vector3[] cylindrPosition;
  public Transform plane;
  public EventHolder eventHolder;
  public SceneManager sceneManager;

  private GameObject[] matrix;
  private bool _initError = false;
  private List<ColorChecker> _colorCheker = new List<ColorChecker>();
  private bool sphereExist = false;

  public List<ColorChecker> ColorCheker
  {
    get { return _colorCheker; }
  }
  #endregion

  private void OnDisable()
  {
    ApplicationContainer.Instance.eventHolder.restoreColorEvent -= ResetColorCheker;
  }

  private void Start()
  {
    InitScene();
  }

  private void InitScene()
  {
    InitLogicObjects();
    InitPhysicalObjects();
  }

  private void InitLogicObjects()
  {
    eventHolder = new GameObject("EventHolder").AddComponent<EventHolder>();
    ApplicationContainer.Instance.eventHolder.restoreColorEvent += ResetColorCheker;
    sceneManager = new GameObject("SceneManager").AddComponent<SceneManager>();
    InitColorChecker();
  }

  private void InitColorChecker()
  {
    for(int i = 0; i < Enum.GetValues(typeof(CylinderType)).Length - 1; i++)
    {
      _colorCheker.Add(new ColorChecker((CylinderType)i));
    }
  }

  private void InitPhysicalObjects()
  {
    if(cylindrPosition != null && cylindrPosition.Length > 0)
    {
      matrix = new GameObject[4];

      List<int> indexes = new List<int>();

      for(int i = 0; i < cylindrPosition.Length; i++)
      {
        if(cylinderPrefab != null)
        {
          matrix[i] = Instantiate(cylinderPrefab);
          matrix[i].transform.position = cylindrPosition[i];

          if(plane != null)
          {
            matrix[i].transform.parent = plane;
          }
          else
          {
            Debug.LogError(string.Format("[{0}][InitScene]ERROR: plane is empty!", this.GetType()));
          }

          int randomColorIndex = UnityEngine.Random.Range(0, 2);

          if(indexes.FirstOrDefault(elemen=>elemen == randomColorIndex) == -1)
          {
            indexes.Add(randomColorIndex);
          }

          CylinderController cController = matrix[i].GetComponent<CylinderController>();

          if(cController != null)
          {
            cController.Init((CylinderType)randomColorIndex);
          }
          else
          {
            Debug.LogError(string.Format("[{0}][InitScene]PREFAB ERROR: Don't get \"CylinderController\"component!", this.GetType()));
          }

          matrix[i].name = string.Format("Cylinder_{0}", ((CylinderType)randomColorIndex).ToString());
        }
        else
        {
          Debug.LogError(string.Format("[{0}][InitScene]ERROR: cylinderPrefab is empty!", this.GetType()));
          _initError = true;
          break;
        }
      }

      if(indexes.Count != Enum.GetValues(typeof(CylinderType)).Length-1)
      {
        for(int i = 0; i < Enum.GetValues(typeof(CylinderType)).Length-1; i++)
        {
          matrix[matrix.Length-1-i].GetComponent<CylinderController>().Init((CylinderType)i);
        }
      }
    }
    else
    {
      Debug.LogError(string.Format("[{0}][InitScene]ERROR: cylindrPosition is empty!", this.GetType()));

      if(!_initError)
      {
        _initError = true;
      }
    }
  }

  private void ResetColorCheker()
  {
    if(_colorCheker != null)
    {
      for(int i = 0; i < _colorCheker.Count; i++)
      {
        _colorCheker[i] = new ColorChecker(_colorCheker[i].Color);
      }
    }

    sphereExist = false;
  }

  public void CheckColor(CylinderType color, GameObject cylinder)
  {
    if(!sphereExist)
    {
      ColorChecker checker = _colorCheker.FirstOrDefault(element => element.Color == color);

      if(checker != null)
      {
        if(!checker.Exist)
        {
          checker.Exist = !checker.Exist;
          checker.Cylindr = cylinder;
        }
      }

      int counter = 0;

      for(int i = 0; i < _colorCheker.Count; i++)
      {
        if(_colorCheker[i].Exist)
        {
          counter++;
        }
        else
        {
          break;
        }
      }

      if(counter == _colorCheker.Count)
      {
        Vector3 positionA = Vector3.zero;
        Vector3 positionB = Vector3.zero;
        ColorChecker cheker = _colorCheker.FirstOrDefault(element=>element.Color == CylinderType.Red);

        if(cheker != null)
        {
          positionA = cheker.Cylindr.transform.position;
        }

        cheker = null;
        cheker = _colorCheker.FirstOrDefault(element => element.Color == CylinderType.Green);

        if(cheker != null)
        {
          positionB = cheker.Cylindr.transform.position;
        }

        if(positionA != Vector3.zero && positionB != Vector3.zero)
        {
          GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
          sphere.name = "Sphere";
          Destroy(sphere.GetComponent<SphereCollider>());
          sceneManager.StartMoveSphere(sphere, positionA, positionB);
          sphereExist = true;
        }
      }
    }
  }
}