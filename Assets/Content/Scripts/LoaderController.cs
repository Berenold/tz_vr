﻿using UnityEngine;

public class LoaderController: MonoBehaviour
{
  public GameObject progressBar;
  public Material material;

  public void InitProgressPar()
  {
    if(progressBar != null)
    {
      Material newMaterial = new Material(material);
      newMaterial.SetFloat("_Cutoff", 0.001f);
      progressBar.GetComponent<Renderer>().sharedMaterial = newMaterial;
      progressBar.SetActive(false);
    }
    else
    {
      Debug.LogError(string.Format("[{0}][InitProgressPar]ERROR: progressBar is empty!", this.GetType()));
    }
  }
}