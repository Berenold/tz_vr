﻿using UnityEngine;
using System;

public class EventHolder: MonoBehaviour
{
  public Action restoreColorEvent;
  public void RestoreColor()
  {
    if(restoreColorEvent != null)
    {
      restoreColorEvent();
    }
  }
}