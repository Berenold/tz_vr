﻿using UnityEngine;

public class CameraController: MonoBehaviour
{
  public void FindeObject()
  {
    Vector3 forward = this.transform.TransformDirection(Vector3.forward);
    RaycastHit Hit;

    if(Physics.Raycast(transform.position, forward, out Hit, Mathf.Infinity))
    {
      if(Hit.transform.tag == "DinamicObject")
      {
        ApplicationContainer.Instance.sceneManager.DoLoader(Hit.transform.GetComponent<CylinderController>().loaderController.progressBar);
      }
    }
  }
}