﻿using UnityEngine;
using System.Collections;

public class SceneManager: MonoBehaviour
{
  #region Variable part
  private Camera _camera;
  private GameObject _sphere;
  private bool _error = false;
  private bool _moveCamera = false;
  private Vector3 _sphereStartPosition;
  private Vector3 _sphereEndPosition;
  private Vector3 _sphereBending;
  private float _timeToTravel = 3.0f;
  #endregion

  private void OnEnable()
  {
    _camera = Camera.main;
    CheckErrors();
  }

  private void CheckErrors()
  {
    if(_camera == null)
    {
      _error = false;
    }
  }

  private void Update()
  {
    if(!_error)
    {
      if(_camera.GetComponent<CameraController>() != null)
      {
        _camera.GetComponent<CameraController>().FindeObject();
      }
      else
      {
        Debug.LogError(string.Format("[{0}][Update]ERROR: MainCamera doesn't have CameraController!", this.GetType()));
        _error = true;
      }

      #if UNITY_EDITOR
      if(Input.GetMouseButtonDown(0))
      {
        _moveCamera = !_moveCamera;
      }
      #endif

      #if UNITY_IOS || UNITY_ANDROID
      if(Input.touchCount > 0)
      {
        if(Input.GetTouch(0).phase == TouchPhase.Ended)
        {
          _moveCamera = !_moveCamera;
        }
      }
      #endif

      if(_moveCamera)
      {
        _camera.transform.Translate(Vector3.forward * Time.deltaTime, Space.Self);
      }
    }
  }

  public void DoLoader(GameObject progressBar)
  {
    if(!progressBar.transform.parent.transform.parent.GetComponent<CylinderController>().IsColorMaterial)
    {
      progressBar.SetActive(true);
      StartCoroutine("LoaderLogic", progressBar);
    }
  }

  private IEnumerator LoaderLogic(GameObject progressBar)
  {
    float revealOffset = 0.001f;

    while(revealOffset != 2f)
    {
      revealOffset += Time.deltaTime;

      if(revealOffset > 2)
      {
        revealOffset = 2;
      }

      progressBar.GetComponent<Renderer>().sharedMaterial.SetFloat("_Cutoff", revealOffset / 2);
      progressBar.transform.parent.LookAt(_camera.transform);
      yield return null;
    }

    progressBar.SetActive(false);
    progressBar.transform.parent.transform.parent.GetComponent<CylinderController>().ChangeMaterialOnHit();
  }

  public void StartMoveSphere(GameObject newSphere, Vector3 positionA, Vector3 positionB)
  {
    if(newSphere != null)
    {
      _sphere = newSphere;
      _sphereStartPosition = positionA;
      _sphereEndPosition = positionB;
      _sphereBending = new Vector3((positionA.x + positionB.x) / 2 * -1, 0, (positionA.z + positionB.z) / 2 * -1);
      StartCoroutine("GetSpherePath");
    }
    else
    {
      _error = true;
      Debug.LogError(string.Format("[{0}][StartMoveSphere]ERROR: \"newSphere\" is empty!", this.GetType()));
    }
  }

  private IEnumerator GetSpherePath()
  {
    float timeStamp = Time.time;

    while(Time.time < timeStamp + _timeToTravel)
    {
      Vector3 currentPos = Vector3.LerpUnclamped(_sphereStartPosition, _sphereEndPosition, (Time.time - timeStamp) / _timeToTravel);
      float x = _sphereBending.x * Mathf.Sin(Mathf.Clamp01((Time.time - timeStamp) / _timeToTravel) * Mathf.PI);
      float y = _sphereBending.y * Mathf.Sin(Mathf.Clamp01((Time.time - timeStamp) / _timeToTravel) * Mathf.PI);
      float z = _sphereBending.z * Mathf.Sin(Mathf.Clamp01((Time.time - timeStamp) / _timeToTravel) * Mathf.PI);
      currentPos += new Vector3(x, y, z);
      _sphere.transform.position = currentPos;
      yield return null;
    }

    Destroy(_sphere);
    ApplicationContainer.Instance.eventHolder.RestoreColor();
  }
}