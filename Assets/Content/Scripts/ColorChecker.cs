﻿using UnityEngine;

public class ColorChecker
{
  private bool _exist;
  private CylinderType _color;
  private GameObject _cylindr;

  public ColorChecker()
  {
    _exist = false;
    _color = CylinderType.Default;
    _cylindr = null;
  }

  public ColorChecker(CylinderType color)
  {
    _exist = false;
    _color = color;
    _cylindr = null;
  }

  public bool Exist
  {
    get { return _exist; }
    set { _exist = value; }
  }

  public CylinderType Color
  {
    get { return _color; }
    set { _color = value; }
  }

  public GameObject Cylindr
  {
    get { return _cylindr; }
    set { _cylindr = value; }
  }
}